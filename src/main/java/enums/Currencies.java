package enums;

public enum Currencies {

    USD("USD"), EUR("EUR");

    private final String currency;

    Currencies(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }
}
