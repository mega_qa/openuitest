package pages.openBank;

import com.codeborne.selenide.ElementsCollection;
import enums.Currencies;
import enums.MarketActions;
import impl.OpenBankMainPageXpathConfiguratorImpl;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class OpenBankMainPage {

    public ElementsCollection exchangeRates(Currencies currency, MarketActions marketAction) {
        OpenBankMainPageXpathConfiguratorImpl openBankMainPageXpathConfigurator = new OpenBankMainPageXpathConfiguratorImpl();

        return $$(By.xpath(openBankMainPageXpathConfigurator.configureXPath(currency, marketAction)));
    }
}
