package services;

import java.text.ParseException;

public interface Comparator <T> {
    boolean firstNumberIsGreaterThenSecond(T firstValue, T secondValue) throws ParseException;
}
