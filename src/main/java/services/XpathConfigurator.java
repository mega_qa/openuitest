package services;

public interface XpathConfigurator <T, S> {
     String configureXPath(T currency, S marketActions);
}
