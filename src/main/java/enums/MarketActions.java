package enums;

public enum MarketActions {

    BANK_BUYING(1), BANK_SELLING(3);

    private final int value;

    MarketActions(int value) {
        this.value = value;
    }

    public int getMarketAction() {
        return value;
    }

}
