package pages.google;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.element;
import static com.codeborne.selenide.Selenide.page;

public class GoogleMainPage {
    public GoogleResultsPage search(String query) {

        element(By.xpath("//input[@title = 'Поиск']"))
                .setValue(query)
                .pressEnter();

        return page(GoogleResultsPage.class);
    }
}
