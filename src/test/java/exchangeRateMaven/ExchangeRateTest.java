package exchangeRateMaven;

import com.codeborne.selenide.Configuration;
import enums.Currencies;
import enums.MarketActions;
import impl.NumberComparatorImpl;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.google.GoogleMainPage;
import pages.google.GoogleResultsPage;
import pages.openBank.OpenBankMainPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.open;

/**
 * 2. UI тестирование
 *
 * Задача:
 * ·         запустить Chrome
 * ·         открыть https://www.google.com/
 * ·         написать в строке поиска «Открытие»
 * ·         нажать Поиск
 * ·         проверить, что результатах поиска есть https://www.open.ru
 * ·         перейти на сайт https://www.open.ru
 * ·         проверить в блоке «Курс обмена в интернет-банке», что курс продажи больше курса покупки, для USD и для EUR.
 */
public class ExchangeRateTest {

    //Тестовые константы
    private static final String SEARCH_PAGE_URL = "https://www.google.com/";
    private static final String SEARCH_VALUE = "Открытие";
    private static final String SEARCH_RESULT_VALUE = "www.open.ru";

    @BeforeClass
    public void setup() {
        Configuration.browser = "chrome";
    }

    @Test
    public void enterMessageToSearchBarAndCheckFoundResults() {

        OpenBankMainPage openBankMainPage = new OpenBankMainPage();
        NumberComparatorImpl numberComparator = new NumberComparatorImpl();


        //Открытие браузера и переход на страницу поиска
        GoogleMainPage googleMainPage = open(SEARCH_PAGE_URL, GoogleMainPage.class);

        //Поиск значений, проверка их наличия в результатах выдачи и переход на найденную страницу
        GoogleResultsPage googleResultsPage = googleMainPage.search(SEARCH_VALUE);
        googleResultsPage
                .results().get(0)
                .shouldHave(text(SEARCH_RESULT_VALUE))
                .click();

        //Проверка, что продажа валюты банком осуществляется дороже, чем покупка
        Assert.assertTrue(numberComparator.firstNumberIsGreaterThenSecond(
                openBankMainPage.exchangeRates(Currencies.USD, MarketActions.BANK_SELLING).get(0).innerText(),
                openBankMainPage.exchangeRates(Currencies.USD, MarketActions.BANK_BUYING).get(0).innerText())
        );
        Assert.assertTrue(numberComparator.firstNumberIsGreaterThenSecond(
                openBankMainPage.exchangeRates(Currencies.EUR, MarketActions.BANK_SELLING).get(0).innerText(),
                openBankMainPage.exchangeRates(Currencies.EUR, MarketActions.BANK_BUYING).get(0).innerText())
        );
    }
}
