package impl;

import enums.Currencies;
import enums.MarketActions;
import services.XpathConfigurator;

public class OpenBankMainPageXpathConfiguratorImpl implements XpathConfigurator <Currencies, MarketActions> {

    /**
     * Configures XPath using currency and market action (buying or selling).
     *
     * @param currency - ENUM with currency value
     * @param marketActions - ENUM with market action value
     * @return - final configured XPath
     */
    @Override
    public String configureXPath(Currencies currency, MarketActions marketActions) {
        String xpath =
                "//table//span[text() = '"
                        + currency.getCurrency()
                        + "']/../../following-sibling::td["
                        + marketActions.getMarketAction()
                        + "]//span";
        return xpath;
    }
}
