package impl;

import services.Comparator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

public class NumberComparatorImpl implements Comparator <String> {

    /**
     * Get 2 Strings with numbers, parse and compare them.
     *
     * @param first - first number in String
     * @param second - second number in String
     * @return - return comparison value
     */
    @Override
    public boolean firstNumberIsGreaterThenSecond(String first, String second){

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        DecimalFormat format = new DecimalFormat("0.##");
        format.setDecimalFormatSymbols(symbols);

        float firstNum = 0;
        float secondNum = 0;
        try {
            firstNum = format.parse(first).floatValue();
            secondNum = format.parse(second).floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return firstNum > secondNum;
    }
}
