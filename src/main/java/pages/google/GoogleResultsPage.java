package pages.google;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class GoogleResultsPage {
    public ElementsCollection results() {
        return $$(By.xpath("//div[@class = 'rc']//a[@href='https://www.open.ru/']"));
    }
}
